## Lumen Docker
Lumen Docker image template based on php:7.2-apache

### Instructions

### to build local
```
cd src
composer install
cd ../
docker build -t lumen-docker .
docker run -d -p 80:80 --name lumen_docker lumen-docker
```

### Complete docker command list
https://docs.docker.com/engine/reference/commandline/docker/

### Docker compose
https://docs.docker.com/compose/reference/overview/

### to build local using docker-compose
If you want to change port create .env file in same directory and set there

```
docker-compose up -d
```

### build in gitlab CI CD
download this repo and upload as new repo on gitlab.
change .gitlab-ci.yml as you desire.
tag your repo to build docker image

### run docker image from gitlab
Docker image name here
https://gitlab.com/herzcthu/lumen-docker/container_registry

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/herzcthu/lumen-docker:v1.0.0
docker run -d -p 80:80 --name lumen_docker registry.gitlab.com/herzcthu/lumen-docker:v1.0.0
```
